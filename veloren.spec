# Note: requires internet access during build procces because of Rust nightly and git-lfs

# Optimize for build time or performance
%bcond_without release_build

%global debug_package %{nil}

%global commit  6389df7685463d9f3c0875ba6e354ec7685bf7d3
%global shortcommit %(c=%{commit}; echo ${c:0:7})
%global date    20200828

%global uuid    net.%{name}.%{name}

Name:           veloren
Version:        0.7.1
Release:        1%{date}git%{shortcommit}%{?dist}
Summary:        Multiplayer voxel RPG written in Rust

License:        GPLv3+
URL:            https://veloren.net/
Source0:        https://gitlab.com/veloren/veloren/-/archive/%{commit}/%{name}-%{version}.%{date}git%{shortcommit}.tar.gz

# Target stable Rust
# * https://gitlab.com/veloren/veloren/issues/264
#BuildRequires:  cargo >= 1.49
#BuildRequires:  rust >= 1.49

BuildRequires:  gcc
BuildRequires:  git-lfs
%if 0%{?epel}
BuildRequires:  python3
%endif
BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib
BuildRequires:  pkgconfig(alsa)
BuildRequires:  pkgconfig(atk)
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(gdk-3.0)
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(libudev)
BuildRequires:  pkgconfig(pango)
BuildRequires:  pkgconfig(xcb)

Requires:       %{name}-data = %{version}-%{release}
Requires:       hicolor-icon-theme

%if 0%{?fedora} || 0%{?rhel} >= 8
Recommends:     %{name}-chat-cli%{?_isa} = %{version}-%{release}
Recommends:     %{name}-server-cli%{?_isa} = %{version}-%{release}
%endif

%description
Veloren is a multiplayer voxel RPG written in Rust. It is inspired by games such
as Cube World, Legend of Zelda: Breath of the Wild, Dwarf Fortress and
Minecraft.


# Data package
%package        data
Summary:        Data files for %{name}
BuildArch:      noarch

Requires:       %{name} = %{version}-%{release}

%description    data
Data files for %{name}.


# Server CLI package
%package        server-cli
Summary:        Standalone server for %{name}

%if 0%{?fedora} || 0%{?rhel} >= 8
Recommends:     %{name}-chat-cli%{?_isa} = %{version}-%{release}
%endif

%description    server-cli
Standalone server for %{name}.


# Chat CLI package
%package        chat-cli
Summary:        Standalone chat for %{name}

%if 0%{?fedora} || 0%{?rhel} >= 8
Recommends:     %{name}-server-cli%{?_isa} = %{version}-%{release}
%endif

%description    chat-cli
Standalone chat for %{name}.


%prep
%autosetup -n %{name}-%{commit} -p1

# Unoptimize dev/debug builds to speed up build process
#sed -i 's/opt-level = 3/opt-level = 0/' Cargo.toml
#sed -i 's/opt-level = 2/opt-level = 0/g' Cargo.toml

# Use recommended upstream veloren version of Rust toolchain
rust_toolchain=$(cat rust-toolchain)

git clone https://gitlab.com/veloren/veloren.git
curl https://sh.rustup.rs -sSf | sh -s -- --profile minimal --default-toolchain ${rust_toolchain} -y

# Sync to avoid different builds
pushd %{name}
  git reset --hard %{commit}
popd


%build
#VELOREN_ASSETS=assets
%if %{with release_build}
  pushd %{name}
    $HOME/.cargo/bin/cargo build --release
  popd
%else
  pushd %{name}/voxygen
    $HOME/.cargo/bin/cargo build
  popd
%endif


%install
# Disable cargo install until bug will been fixed in upstream
#$HOME/.cargo/bin/cargo install --root=%{buildroot}%{_prefix} --path=.
pushd %{name}
  %if %{with release_build}
    install -m 0755 -Dp target/release/%{name}-voxygen %{buildroot}%{_bindir}/%{name}-voxygen

    # Standalone server
    install -m 0755 -Dp target/release/%{name}-server-cli %{buildroot}%{_bindir}/%{name}-server-cli

    # Standalone chat
    install -m 0755 -Dp target/release/%{name}-chat-cli %{buildroot}%{_bindir}/%{name}-chat-cli
  %else
    install -m 0755 -Dp target/debug/%{name}-voxygen %{buildroot}%{_bindir}/%{name}-voxygen
  %endif
  mkdir -p        %{buildroot}%{_datadir}/%{name}
  cp -ap assets/  %{buildroot}%{_datadir}/%{name}/

  # Install desktop file, icon and appdata manifest
  # * https://gitlab.com/veloren/veloren/merge_requests/654
  install -m 0644 -Dp assets/voxygen/%{uuid}.metainfo.xml %{buildroot}%{_metainfodir}/%{uuid}.appdata.xml
  install -m 0644 -Dp assets/voxygen/%{uuid}.desktop %{buildroot}%{_datadir}/applications/%{uuid}.desktop
  install -m 0644 -Dp assets/voxygen/%{uuid}.png %{buildroot}%{_datadir}/icons/hicolor/256x256/apps/%{uuid}.png
popd

strip --strip-all %{buildroot}%{_bindir}/*


%check
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/*.xml
desktop-file-validate %{buildroot}%{_datadir}/applications/*.desktop


%files
%license LICENSE
%doc README.md CONTRIBUTING.md
%{_bindir}/%{name}-voxygen

%files data
%{_datadir}/%{name}/
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/*/*.png
%{_metainfodir}/*.xml

%if %{with release_build}
%files server-cli
%license LICENSE
%doc README.md
%{_bindir}/%{name}-server-cli

%files chat-cli
%license LICENSE
%doc README.md
%{_bindir}/%{name}-chat-cli
%endif


%changelog
* Sun Aug 30 2020 Artem Polishchuk <ego.cordatus@gmail.com> 0.7.1-120200828git6389df7
- new package built with tito

* Fri Aug 28 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.7.0-2.20200828git6389df7
- Update to latest git snapshot

* Sun Aug 23 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.7.0-1.20200823git877eaee
- Update to latest git snapshot

* Sat Aug 15 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.6.0-1.20200815git8f8b20c
- Update to 0.7

* Thu Aug 13 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.6.0-1.20200813git75c1d44
- Update to latest git snapshot

* Mon Jul 20 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.6.0-1.20200720git62005e9
- Update to latest git snapshot

* Thu Jul 16 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.6.0-1.20200716gita89c88d
- Update to latest git snapshot

* Fri Jun 19 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.6.0-1.20200619gitaabf68e
- Update to latest git snapshot

* Fri Jun 12 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.6.0-1.20200612git4822150
- Update to latest git snapshot

* Wed Jun 10 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.6.0-1.20200610gitde849ad
- Update to latest git snapshot

* Sat Jun 06 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.6.0-1.20200606git521688f
- Update to latest git snapshot

* Mon Jun 01 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.6.0-1.20200601git43dc232
- Update to latest git snapshot

* Wed May 27 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.6.0-1.20200527git2ad8172
- Update to latest git snapshot

* Fri May 22 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.6.0-1.20200522git49d4141
- Update to latest git snapshot

* Sat May 16 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.6.0-1.20200516git6c4e3eb
- Update to latest git snapshot

* Fri May 15 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200514git71dd520
- Update to latest git snapshot

* Thu May 14 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200514git51eb928
- Update to latest git snapshot

* Wed May 13 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200511git7703bc1
- Update to latest git snapshot

* Mon May 11 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200511git0ee0949
- Update to latest git snapshot

* Sun May 10 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200510gita8b1520
- Update to latest git snapshot

* Wed Apr 29 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200429git19ce03d
- Update to latest git snapshot

* Sun Apr 26 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200426git6d7cb86
- Update to latest git snapshot

* Sat Apr 25 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200426gite690efe
- Update to latest git snapshot

* Fri Apr 24 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200424gitf3200ec
- Update to latest git snapshot

* Thu Apr 16 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200416git51126da
- Update to latest git snapshot

* Sun Apr 12 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200413git1dd1318
- Update to latest git snapshot

* Fri Apr 10 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200410git198b8a4
- Update to latest git snapshot

* Thu Apr 09 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200409git9427d51
- Update to latest git snapshot

* Sat Apr 04 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200405git32db0df
- Update to latest git snapshot

* Thu Apr 02 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200403git22f3319
- Update to latest git snapshot

* Wed Apr 01 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200402gitf34d4b3
- Update to latest git snapshot

* Sat Mar 28 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200328git976043d
- Update to latest git snapshot

* Thu Mar 26 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200326git63d5e19
- Update to latest git snapshot

* Mon Mar 23 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200323git30e6bbc
- Update to latest git snapshot

* Fri Mar 20 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200320git0c9ef6d
- Update to latest git snapshot

* Wed Mar 11 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200311git875ae6c
- Update to latest git snapshot

* Fri Mar 06 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200306git511bbd3
- Update to latest git snapshot

* Fri Feb 21 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200221git13cbef7
- Update to latest git snapshot

* Fri Feb 14 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200214git1c6460f
- Update to latest git snapshot

* Thu Feb 06 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200206gite524e86
- Update to latest git snapshot

* Sat Feb 01 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.5.0-1.20200201git7940ced
- Update to latest git snapshot

* Sat Jan 25 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.4.0-2.20200125gite716ca2
- Update to latest git snapshot

* Tue Jan 21 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.4.0-2.20200122git22b556b
- Update to latest git snapshot

* Sun Jan 19 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.4.0-2.20200119git259879d
- Update to latest git snapshot

* Tue Jan 14 2020 ElXreno <elxreno@gmail.com> - 0.4.0-2.20200114git8d2c696
- Updated to commit 8d2c69673dd63e3cb689f1df5574eabfed7826af

* Thu Jan 09 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.4.0-1.20200109git4e7c955
- Update to latest git snapshot

* Sun Jan 05 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.4.0-1.20200105git4fa0515
- Update to latest git snapshot

* Wed Jan 01 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.4.0-1.20200101git7313ec6
- Update to latest git snapshot

* Fri Dec 20 2019 Artem Polishchuk <ego.cordatus@gmail.com> - 0.4.0-1.20191220git6e64924
- Update to latest git snapshot

* Sat Dec 07 2019 Artem Polishchuk <ego.cordatus@gmail.com> - 0.4.0-1.20191207git3e2fe36
- Update to latest git snapshot

* Fri Nov 29 2019 Artem Polishchuk <ego.cordatus@gmail.com> - 0.4.0-1.20191128gitb7a084c
- Update to latest git snapshot
- Add desktop file, icon and appdata manifest
- Split into separate data package

* Sat Nov 23 2019 Artem Polishchuk <ego.cordatus@gmail.com> - 0.4.0-1.20191123git01aa89a
- Update to latest git snapshot

* Thu Nov 21 2019 Artem Polishchuk <ego.cordatus@gmail.com> - 0.4.0-1.20191121git905b3ac
- Update to latest git snapshot

* Wed Nov 13 2019 Artem Polishchuk <ego.cordatus@gmail.com> - 0.4.0-1.20191112giteb7b55d
- Update to latest git snapshot
- Add chat-cli package
- Minor packaging fixes

* Mon Nov 11 2019 Artem Polishchuk <ego.cordatus@gmail.com> - 0.4.0-1.20191109git5fe1eec
- Update to latest git snapshot
- Thanks @ElXreno <elxreno@gmail.com> for help with debugging build

* Sat Sep 07 2019 Artem Polishchuk <ego.cordatus@gmail.com> - 0.3.0-2.20190907git92c0edc
- Update to latest git snapshot

* Fri Mar 29 2019 Artem Polishchuk <ego.cordatus@gmail.com> - 0.3.0-1.20190906gitd41d020
- Initial package
